#ifndef _CHAT_MESSAGE_OBJECT_H_
#define _CHAT_MESSAGE_OBJECT_H_

#include <string>
#include <vector>
#include <iomanip>
#include <ctime>
#include <chrono>
#include <iostream>


#include "type_msg.h"

//Message class for better organization of message in chatroom
//Unfinished

/*
	msg_format
	|--16bits--||--8bits--||-8bits-||---------64bits---------||--8bits----||--(y)bits-------||--------(x) bits----------|
  {msg length:n}{magic num}{  type }{          time_t        }{ cli_id_len}{   name(id)     }{          msg               }
*/


class Chat_Msg;

std::ostream& 
operator<<(std::ostream& os, const Chat_Msg& msg);

class Chat_Msg {
public:
	Chat_Msg();
	Chat_Msg(Chat_Msg& msg) = default;
	Chat_Msg(Chat_Msg&& msg) = default;
	bool getDatagram(std::vector<uint8_t>& Dest, cmsg_t spec_type);
	/*Need the caller to read correct number of bytes from socket before*/
	bool setMsg(const std::vector<uint8_t>& Src, cmsg_t& get_type);
	bool setMsg(const std::string& au, const std::string& msg);

	friend std::ostream&
		operator<<(std::ostream& os, const Chat_Msg& msg);
	static const uint8_t magic_number = 72;
	static const uint16_t BufSize = 1024; //1 KB
	//for server to forward msg
	static bool modDataramType(std::vector<uint8_t>& dg, cmsg_t newType);
private:
	void set_time_t() {
		std::chrono::system_clock::time_point
			tp = std::chrono::system_clock::now();

		this->time_created=
			std::chrono::system_clock::to_time_t(tp);
	}
	std::string message;
	std::string author;
	std::time_t time_created;
};
#endif // !_CHAT_MESSAGE_OBJECT_H_
