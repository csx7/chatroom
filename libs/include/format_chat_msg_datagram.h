#ifndef _FORMAT_CHAT_MSG_DATAGRAM_
#define _FORMAT_CHAT_MSG_DATAGRAM_
/*
	msg_format
	|--16bits--||-8bits-||--------(n-1) bits----------|
  {msg length:n}{  type }{          msg               }
*/

#include <vector>
#include <iostream>
#include "unp.h"
#include "type_msg.h"

template<typename T>
bool 
Frecvmsg(int confd, T& type, std::vector<uint8_t>& origin_msg)
{
	static const int bufsize = 1024; // 1 KB
	uint8_t buffer[bufsize];

	cmsg_len len;
	origin_msg.clear();

	if (Readn(confd, &len, sizeof(len)) != sizeof(len)) {
		return false;
	}
	
	len = ntohs(static_cast<uint16_t>(len));
	if (len > bufsize) {
		std::cerr << "len: " << len << " is invalid" << std::endl;
		return false;
	}

	//BUG!
	//origin_msg.reserve(len);
	if (Readn(confd, buffer, len) != len) {
		return false;
	}
	for (int i = 0; i < len; ++i) {
		origin_msg.push_back(buffer[i]);
	}
	return getFrecv_msg(type, origin_msg);;
}


//overload
template<typename T>
bool
getFsend_msg(std::vector<uint8_t>& Dest_format_msg, T type, const std::vector<uint8_t>& msg)
{
	typedef T cmsg_tt;
	Dest_format_msg.clear(); //IMPORTANT
	//std::vector<uint8_t> format_msg;

	if (msg.size() == 0) {
		std::cerr << "Fsend_chat(): empty msg\n" << std::endl;
		return false;
	}

	cmsg_len len = sizeof(cmsg_tt) + msg.size();
	Dest_format_msg.reserve(sizeof(len) + len);

	cmsg_len netendian_len = static_cast<cmsg_len>(htons(static_cast<uint16_t>(len)));

	uint8_t* p = static_cast<uint8_t*>((void*)(&netendian_len));
	for (int i = 0; i < sizeof(netendian_len); ++i) {
		Dest_format_msg.push_back(*p++);
	}

	p = static_cast<uint8_t*>((void*)&type);//IMPORTANT
	for (int i = 0; i < sizeof(cmsg_tt); ++i) {
		Dest_format_msg.push_back(*p++);
	}

	for (char c : msg) {
		Dest_format_msg.push_back(c);
	}

	return false;
}


//overload
template<typename T>
bool
getFsend_msg(std::vector<uint8_t>& Dest_format_msg, T type, const std::string& msg)
{
	typedef T cmsg_tt;
	Dest_format_msg.clear();  //IMPORTANT
	//std::vector<uint8_t> format_msg;

	if (msg.size() == 0) {
		std::cerr << "Fsend_chat(): empty msg\n" << std::endl;
		return false;
	}

	cmsg_len len = sizeof(cmsg_tt) + msg.size();
	Dest_format_msg.reserve(sizeof(len) + len);

	cmsg_len netendian_len = static_cast<cmsg_len>(htons(static_cast<uint16_t>(len)));

	uint8_t* p = static_cast<uint8_t*>((void*)(&netendian_len));
	for (int i = 0; i < sizeof(netendian_len); ++i) {
		Dest_format_msg.push_back(*p++);
	}

	p = static_cast<uint8_t*>((void*)&type);//IMPORTANT
	for (int i = 0; i < sizeof(cmsg_tt); ++i) {
		Dest_format_msg.push_back(*p++);
	}

	for (char c : msg) {
		Dest_format_msg.push_back(c);
	}

	return false;
}


template<typename T>
bool 
getFrecv_msg(T& type, std::vector<uint8_t>& origin_msg)
{
	typedef T cmsg_tt;
	if (origin_msg.size() < sizeof(cmsg_tt) + 1) {
		return false;
	}

	memcpy(&type, &origin_msg[0], sizeof(cmsg_tt));//not neccesary
	origin_msg.erase(origin_msg.begin(), origin_msg.begin() + sizeof(cmsg_tt));
	return true;
}


void print_vec_msg(const std::vector<uint8_t>& vecmsg);
#endif // !_FORMAT_CHAT_MSG_DATAGRAM_
