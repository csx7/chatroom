#ifndef _CHAT_MSG_TYPE_H
#define _CHAT_MSG_TYPE_H

// Basic and rough message type
// Need to be improve, like Chat_Msg.h
typedef uint8_t cmsg_t;
typedef uint16_t cmsg_len;


enum class server_msgtype: cmsg_t {
	PUSH, GET, ERROR
};

enum class client_msgtype : cmsg_t {
	PUSH, GET, ATTEND_ROOM
};
#endif