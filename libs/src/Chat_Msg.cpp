#include "Chat_Msg.h"
#include <arpa/inet.h>

std::ostream&
operator<<(std::ostream& os, const Chat_Msg& msg)
{
	os << '[' << std::put_time(std::localtime(&msg.time_created), "%F %T") << ']'
	   << msg.author << ": " << msg.message << ';';
	return os;
}


Chat_Msg::Chat_Msg(){}


bool 
Chat_Msg::getDatagram(vector<uint8_t>& Dest, cmsg_t spec_type)
{
	if (message.empty())
		return false;

	cmsg_len Len;
	Len = sizeof(Len) + sizeof(spec_type) + sizeof(magic_number)
		+ sizeof(time_created) + author.size() + message.size();

	cmsg_len netEndian_Len = static_cast<cmsg_len>(htons(Len));

	uint8_t* lp;
	
	lp = static_cast<uint8_t*>((void*)&netEndian_Len);
	for (int i = 0; i < sizeof(netEndian_Len); ++i) {
		Dest.push_back(*lp++);
	}

	lp = static_cast<uint8_t*>((void*)&magic_number);
	for (int i = 0; i < sizeof(magic_number); ++i) {
		Dest.push_back(*lp++);
	}

	lp = static_cast<uint8_t*>((void*)&spec_type);
	for (int i = 0; i < sizeof(spec_type); ++i) {
		Dest.push_back(*lp++);
	}

	lp = static_cast<uint8_t*>((void*)&time_created);
	for (int i = 0; i < sizeof(time_created); ++i) {
		Dest.push_back(*lp++);
	}

	//name's length
	uint8_t nlen = author.size();
	lp = static_cast<uint8_t*>((void*)&nlen);
	for (int i = 0; i < sizeof(nlen); ++i) {
		Dest.push_back(*lp++);
	}

	Dest.insert(Dest.end(), author.begin(), author.end());

	Dest.insert(Dest.end(), message.begin(), message.end());

	return true;
}