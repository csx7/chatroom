#ifndef _MY_EPOLL_WRAPPER_H_
#define _MY_EPOLL_WRAPPER_H_

#ifdef __cplusplus
extern "C" {
#endif


#include <sys/epoll.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

int Epoll_wait(int epfd, struct epoll_event* evlist, int maxevents, int timeout);


#ifdef __cplusplus
}
#endif

#endif // !_MY_EPOLL_WRAPPER_
