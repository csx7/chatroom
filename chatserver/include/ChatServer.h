#pragma once
/*
	Group Chat Server
	Basic Structure: A Thread for per room, and epoll for chatter in each room
*/

//stdlib
#include <iostream>
#include <thread>
#include <vector>
#include <string>
#include <list>
#include <array>

//Linux epoll
#include <sys/epoll.h>

//customlib
#include "unp.h"
#include "thread_cpp.h"
#include "room.h"
#include "type_msg.h"
#include "test1.h"
#include "format_chat_msg_datagram.h"
#include "myepoll.h"


#ifndef _CHATSERVER_H_
#define _CHATSERVER_H_

class ChatServer {
public:
	ChatServer();
	ChatServer(uint8_t n_room, uint16_t n_maxgroup);
	~ChatServer();
	void run_server(uint16_t port);


private:
	void run_room(room& troom);
	bool send_cli_to_room(int confd);
	int cast_to_clients(std::vector<int>& cli_confds, std::vector<uint8_t>& msg);
	//int rm_cli_from_room(room& r, int confd);
	int net_test(int confd);
	int net_setserver(uint16_t port);
	struct sockaddr_in servaddr;
	int			listenfd;
	uint8_t		n_room;
	uint16_t	n_maxgroup;
	std::vector<joining_thread> rooms;
	std::vector<room> cli_of_rooms;
	std::vector<int> epollfds;
	std::vector<std::mutex> acess_locks;
	std::mutex lock_stdout; /*cout locks*/
};
#endif // !_CHATSERVER_H_
