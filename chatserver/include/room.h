#ifndef _CHAT_ROOM_H
#define _CHAT_ROOM_H_

#include <list>
#include <vector>
#include <iostream>
#include <sys/epoll.h>
#include "thread_cpp.h"
#include "unp.h"


class room {
	/*
	** Protect confds during threads with mutex;
	*/
public:
	room(std::mutex& m, int tepfd) 
		:clients_m(m), room_epollfd(tepfd) {}
	room(room&) = delete;
	room(room&& oldroom):clients_m(oldroom.clients_m) {
		this->cli_confds = std::move(oldroom.cli_confds);
		this->room_epollfd = oldroom.room_epollfd;
	}
	~room() = default;
	int cli_add(int const confd);
	int cli_rm(int const confd);
	bool cli_get(std::vector<int>& cli_confds_cp);
	int get_epollfd() const {
		return room_epollfd;
	}
private:
	std::list<int> cli_confds;
	//std::unique_lock<std::mutex> clients_m;
	std::mutex& clients_m;
	int room_epollfd;
	//std::mutex testm;
};
#endif //!_CHAT_ROOM_H_