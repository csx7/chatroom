#include "myepoll.h"

int 
Epoll_wait(int epfd, struct epoll_event* evlist, int maxevents, int timeout)
{
	int ready_num = 0;
	while (ready_num == 0) {
		ready_num = epoll_wait(epfd, evlist, maxevents, timeout);

		if (ready_num == -1) {
			if (errno == EINTR)
				continue; /* Restart if interrupted by signal */
			else
				return -1;
		}
	}
	return ready_num;
}