#include "ChatServer.h"

static const int bufsize = 1024; //1 KB

ChatServer::ChatServer():ChatServer(10, 100){}

ChatServer::ChatServer(uint8_t n_room, uint16_t n_maxgroup)
	:rooms(n_room), acess_locks(n_room)
{
	this->n_room = n_room;
	this->n_maxgroup = n_maxgroup;
}

ChatServer::~ChatServer()
{
	Close(listenfd);
}

/*
	Get TCP socket and bind it specify inetaddr & port
*/
int
ChatServer::net_setserver(uint16_t port)
{
	listenfd = Socket(AF_INET, SOCK_STREAM, 0);

	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port = htons(port);

	Bind(listenfd, (SA*) &servaddr, sizeof(servaddr));

	return listenfd;
}

/*
	run server: listen to socket(listenfd) and accept
	connection from client
*/
void
ChatServer::run_server(uint16_t port)
{
	using namespace std;
	int confd;
	struct sockaddr_in cliaddr;
	socklen_t cliaddrlen;
	char cliaddrp[INET6_ADDRSTRLEN];

	//create rooms' source
	for (int i = 0, epollfd; i < n_room; ++i) {
		if ((epollfd = epoll_create(n_maxgroup)) == -1) {
			err_quit("ChatServer::run_server(): epollfd");
		}
		epollfds.push_back(epollfd);//epoll file descriptors
		
		cli_of_rooms.emplace_back(std::ref(acess_locks[i]), epollfd); //list of clients
	}

	//create rooms (aka threads)
	for (int i = 0; i < epollfds.size(); ++i) {
		rooms.push_back( 
			joining_thread(&ChatServer::run_room, this, std::ref(cli_of_rooms[i])));
	}

	net_setserver(port);

	Listen(listenfd, LISTENQ);

	lock_stdout.lock();
	std::cout << "Server is set up" << std::endl;
	lock_stdout.unlock();

	for (socklen_t clilen; ; ) {
		clilen = sizeof(cliaddr);
		confd = Accept(listenfd, (SA*) &cliaddr, &clilen);
		
		cliaddrlen = sizeof(cliaddr);
		getpeername(confd, (SA* )&cliaddr, &cliaddrlen);	/* get client's address*/
		Inet_ntop(AF_INET, &cliaddr.sin_addr, cliaddrp, sizeof(cliaddrp));
		printf("Client: %s:%hu\n", cliaddrp, ntohs(cliaddr.sin_port));

		if (send_cli_to_room(confd) == false) {
			Close(confd);
		}
		//net_test(confd);
	}
}



bool			/*caller need to close(confd) if return false*/
ChatServer::send_cli_to_room(int confd)
{
	cmsg_len len;
	client_msgtype mt;
	uint8_t roomid;
	cmsg_len const rlen = sizeof(mt) + sizeof(n_room);

	if (Readn(confd, &len, sizeof(len)) != sizeof(len))
		return false;

	len = ntohs(len);/*IMPORTANT*/
	
	if (len != rlen) {
		std::cerr << "send_cli_to_room(): " << 
			"choose room msg's length is invalid\n" << std::endl;
		return false;
	}


	if (Readn(confd, &mt, sizeof(mt)) != sizeof(mt))
		return false;	
	len -= sizeof(mt);

	if (mt != client_msgtype::ATTEND_ROOM) {
		std::cerr << "send_cli_to_room(): client " << confd <<
			" do not choose a room\n" << std::endl;
		return false;
	}
	
	if (Readn(confd, &roomid, sizeof(roomid)) != sizeof(roomid))
		return false;

	if (!(roomid >= 0 && roomid < n_room)) {
		std::cerr << "send_cli_to_room(): invalid room number: " <<
			roomid << '\n' << std::endl;
		return false;
	}

	//check finished, add the client to roomid
	//struct epoll_event ev;
	//ev.events = EPOLLIN;
	//ev.data.fd = confd;

	//if (epoll_ctl(epollfds[roomid], EPOLL_CTL_ADD, confd, &ev) == -1)
	//	return false;

	cli_of_rooms[roomid].cli_add(confd);
	
	//feedback
	std::string
		fb{ "You are in room " + std::to_string(roomid) + " now\n" };
	server_msgtype servmt = server_msgtype::PUSH;

	std::vector<uint8_t> ffb;
	getFsend_msg(ffb, servmt, fb);

	if (writen(confd, &ffb[0], ffb.size()) != ffb.size()) {
		cli_of_rooms[roomid].cli_rm(confd);
		std::cerr <<
			"send_cli_to_room(): The client can not recv feedback msg\n" << std::endl;
		return false;
	}
	return true;
}

void 
ChatServer::run_room(room& troom)
{
	//get epoll fd
	int tepollfd = troom.get_epollfd();
	//say something at the begin of the task
	lock_stdout.lock();
	std::cout << "Thread room's epollfd: " <<
		tepollfd << '\n' << std::endl;
	lock_stdout.unlock();

	
	std::vector<int> clients;
	std::vector<uint8_t> buffer, msg_send;

	buffer.reserve(bufsize);

	struct epoll_event evlist[n_maxgroup];
	int evready;

	while (true) {
		//epoll_wait
		std::cout << "Epoll waiting :" << tepollfd << std::endl;
		evready = Epoll_wait(tepollfd, evlist, n_maxgroup, -1);
		if (evready == -1) {
			break;
		}
		else if (evready == 0) {
			continue;
		}
		//get current client list
		troom.cli_get(clients);
		
		//get message and broadcast
		for (int j = 0; j < evready; ++j) {
			printf(" fd=%d; events: %s%s%s\n", evlist[j].data.fd,
				(evlist[j].events & EPOLLIN) ? "EPOLLIN " : "",
				(evlist[j].events & EPOLLHUP) ? "EPOLLHUP " : "",
				(evlist[j].events & EPOLLERR) ? "EPOLLERR " : "");
			buffer.clear();

			if (evlist[j].events & EPOLLIN) {
				client_msgtype cmt;
				if (Frecvmsg(evlist[j].data.fd, cmt, buffer) == false) {
					std::cerr << "run room(): confd " << evlist[j].data.fd << std::endl;
					troom.cli_rm(evlist[j].data.fd);
					troom.cli_get(clients);
					std::cout << "remove confd " << evlist[j].data.fd << std::endl;
				}

				server_msgtype smt = server_msgtype::PUSH;
				getFsend_msg(msg_send, smt, buffer);
				cast_to_clients(clients, msg_send);
			}
			else if (evlist[j].events & (EPOLLHUP | EPOLLERR)) {
				//close and remove sick confd
				troom.cli_rm(evlist[j].data.fd);
				//renew clients lists
				troom.cli_get(clients);
			}
		}

		//getchar();
	}
	getchar();
	lock_stdout.lock();
	std::cout << "Thread exist: "<<std::this_thread::get_id() << std::endl;
	lock_stdout.unlock();
}


int 
ChatServer::cast_to_clients(std::vector<int>& cli_confds, std::vector<uint8_t>& msg)
{
	void* msg_p = &msg[0];
	int num = 0;

	for (int confd : cli_confds) {
		if (writen(confd, msg_p, msg.size()) != msg.size()) {
			std::cerr << "cast_to_clients(): confd " << confd;
			continue;
		}
		++num;
	}
	return num;
}

int 
ChatServer::net_test(int confd)
{
	char buf[BUFFSIZE];
	char str[]{ "hello, client\n" };

	cmsg_len len;
	client_msgtype mt;
	//int wn = write(confd, str, sizeof(str));
	//int rn = read(confd, buf, sizeof(buf));
	while (Readn(confd, &len, sizeof(len)) == sizeof(len)) {
		len = ntohs(len);
		std::cout << "msg len: " << len;
		Readn(confd, &mt, sizeof(mt));
		len -= 1;
		Readn(confd, &buf[0], len);

		std::string op;
		switch (mt) {
		case client_msgtype::ATTEND_ROOM:
			op = "ATTEND_ROOM";
			break;
		case client_msgtype::GET:
			op = "GET";
			break;
		case client_msgtype::PUSH:
			op = "PUSH";
			break;
		default:
			op = "error";
			break;
		}
		//buf[rn] = '\0';
		std::cout << "operation: " << op << ' ';
		printf("From Client: %s", buf);
	}
	std::cout << "Client closed the connection" << std::endl;
	Close(confd);
	return 1;
}