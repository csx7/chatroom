#include "room.h"

/*
**On error:		return 0
**On Success:	return 1
*/

using namespace std;
int
room::cli_add(int const confd)
{
	//test
	//cout << "mutex address: " << &clients_m << endl;
	if (confd < 0) {
		err_ret("room::cli_add: \"confd is invalid\"");
		return 0;
	}
	lock_guard<mutex> lck(clients_m);

	//add to epoll in kernel
	struct epoll_event ev;
	ev.events = EPOLLIN;
	ev.data.fd = confd;

	if (epoll_ctl(this->room_epollfd, EPOLL_CTL_ADD, confd, &ev) == -1)
		return 0;
	//add to mailing list
	cli_confds.push_back(confd);
	return 1;
}

int
room::cli_rm(int const confd)
{
	lock_guard<mutex> lck(clients_m);

	/*std::list<int>::iterator it = cli_confds.begin();
	for ( ; it != cli_confds.end(); ++it) {
		if (*it == confd) {
			cli_confds.erase(it);
			break;
		}
	}
	return (cli_confds.end() == it);*/

	//close confd
	Close(confd);
	//remove the confd from mailling list
	cli_confds.remove(confd);

	//remove the confd from epoll interest list
	//maybe unnecessary, because 
	//'Closing a file descriptor 
	//automatically removes it from all of the epoll interest
	//lists of which it is a member.'
	epoll_ctl(this->room_epollfd, EPOLL_CTL_DEL, confd, NULL);
	return 1;
}

bool
room::cli_get(std::vector<int>& cur_cp_confds)
{
	lock_guard<mutex> lck(clients_m);
	cur_cp_confds.clear();
	for (int& fd : cli_confds) {
		cur_cp_confds.push_back(fd);
	}

	return (cur_cp_confds.size() > 0);
}