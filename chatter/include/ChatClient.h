#pragma

#include <iostream>
#include <string>
#include <array>

#include "unp.h"
#include "type_msg.h"
#include "format_chat_msg_datagram.h"
#include "thread_cpp.h"

//	Client
//	Dual threads: Receiver and Sender

#ifndef _CHATCLIENT_H_
#define _CHATCLIENT_H_

class ChatClient {
public:
	ChatClient(const char* straddr, uint16_t servport, std::string nickname, uint8_t roomid);
	~ChatClient();
	
	int ConnServ(const char* straddr, uint16_t servport);
private:
	bool net_attend_room(uint8_t roomid);
	void net_send_msgs(pthread_t recver);
	void net_recv_msgs();
	int net_test();
	int confd;
	struct sockaddr_in servaddr;
	std::string cli_name;
	bool connect_status;
	uint8_t roomid;
	
};
#endif // !_CHATCLIENT_H_
