#client CMakeLists.txt

project(chatclient)

add_executable(client_main "src/main_client.cpp" "src/ChatClient.cpp")
target_include_directories(client_main PRIVATE ${PROJECT_SOURCE_DIR}/include)
target_link_libraries(client_main PRIVATE sub::libs)

find_package(Threads REQUIRED)
target_link_libraries(client_main PRIVATE Threads::Threads)