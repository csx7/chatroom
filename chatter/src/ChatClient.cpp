#include "ChatClient.h"


static const std::string exit_code{ "/b_exit" };


ChatClient::ChatClient(const char* straddr, uint16_t servport, std::string nickname, uint8_t roomid)
{
	connect_status = false;
	cli_name = nickname + ": ";

	this->roomid = roomid;
	//ConnServ(straddr, servport);	
}

ChatClient::~ChatClient()
{
	Close(confd);
}

int
ChatClient::ConnServ(const char* straddr, uint16_t servport)
{
	confd = Socket(AF_INET, SOCK_STREAM, 0);
	
	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(servport);
	Inet_pton(AF_INET, straddr, &servaddr.sin_addr);

	Connect(confd, (SA*)&servaddr, sizeof(servaddr));
	//net_test();
	net_attend_room(roomid);
	connect_status = true;
	std::cout << "Connect to Server" << std::endl;

	joining_thread recver(&ChatClient::net_recv_msgs, this);
	joining_thread sender(&ChatClient::net_send_msgs, this, recver.as_thread().native_handle());
	
	return confd;
}


bool 
ChatClient::net_attend_room(uint8_t roomid)
{
	//roomid
	std::string msg(1, roomid);
	client_msgtype mt = client_msgtype::ATTEND_ROOM;

	std::vector<uint8_t> fmsg;
	
	getFsend_msg(fmsg, static_cast<cmsg_t>(mt), msg);

	if (writen(confd, &fmsg[0], fmsg.size()) != fmsg.size())
		return false;

	cmsg_len len;
	if (Readn(confd, &len, sizeof(len)) != sizeof(len)) {
		return false;
	}
	len = ntohs(static_cast<uint16_t>(len));

	std::vector<uint8_t> fbmsg(len);
	if (Readn(confd, &fbmsg[0], fbmsg.size()) != fbmsg.size()) {
		return false;
	}

	server_msgtype smt;
	getFrecv_msg(smt, fbmsg);

	if (smt == server_msgtype::PUSH) {
		print_vec_msg(fbmsg);
	}
	return true;
}


void
ChatClient::net_send_msgs(pthread_t recver_th)
{
	const int bufsize = 1024; //1 KB
	std::array<char, bufsize> buffer;

	std::string msg;
	std::string smsg;
	
	std::vector<uint8_t> fmsg;
	client_msgtype cmt = client_msgtype::PUSH;
	//getFsend_msg(fmsg, cmt, smsg);
	std::cout << "Enter msg: ";
	while (connect_status) {
		//std::cin >> msg;
		std::cin.getline(&buffer[0], buffer.max_size());
		if (strncmp(&buffer[0], exit_code.c_str(), bufsize) == 0) {
			connect_status = false;
			break;
		}
		getFsend_msg(fmsg, cmt, cli_name +
			std::string(&buffer[0]) );

		if (writen(confd, &fmsg[0], fmsg.size()) != fmsg.size()) {
			connect_status = false;
		}
	}
	
	pthread_cancel(recver_th);
	std::cout << "net_send_msgs(): exist" << std::endl;
}


void 
ChatClient::net_recv_msgs()
{
	std::vector<uint8_t> recvbuf;
	server_msgtype smt;

	while (connect_status) {
		if (Frecvmsg(confd, smt, recvbuf)) {
			std::cout << "\nFrom server: " << "msg_type: " << (int)smt << ";\nmsg: ";
			print_vec_msg(recvbuf);

			std::cout << "\nEnter msg:";
			std::cout.flush();
		}
		else {
			std::cout << "net_recv_msgs(): Frecvmsg() error" << std::endl;
			connect_status = false;
		}
	}

	std::cout << "net_recv_msgs(): exist" << std::endl;
}


int
ChatClient::net_test()
{
	char gbuf[BUFFSIZE]{-1};
	char sbuf[BUFFSIZE]{-1};
	std::string str{ "hello, server!\n" };
	//read(confd, buf, BUFFSIZE);
	//write(confd, str.c_str(), str.size());

	int max_msg_len = BUFFSIZE - sizeof(cmsg_len) - sizeof(client_msgtype);

	char* cpyPointer = &sbuf[sizeof(cmsg_len)];
	client_msgtype mt = client_msgtype::ATTEND_ROOM;

	memcpy(cpyPointer, &mt, sizeof(client_msgtype));
	cpyPointer += sizeof(client_msgtype);
	strcpy(cpyPointer, str.c_str());

	cmsg_len netendian_len = static_cast<cmsg_len>(htons(
		static_cast<cmsg_len>(sizeof(client_msgtype) + str.size() + 1)));
	
	memcpy(sbuf, &netendian_len, sizeof(netendian_len));

	uint32_t len = sizeof(cmsg_len) + sizeof(client_msgtype) + str.size() + 1;


	/*int op;
	for (bool w = true; w; ) {
		if (writen(confd, sbuf, len) != len) {
			w = false;
		}
		std::cout << "Press to send msg" << std::endl;
		op = getchar();
		if (op == 'e') {
			w = false;
		}
	}*/
	
	//fprintf(stdout, "From server: %s", buf);
	std::cout << "client exist\n" << std::endl;
	return 1;
}
