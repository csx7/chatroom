#include <iostream>
#include "ChatClient.h"

using namespace std;

int main(int argc, char** argv)
{
	string saddrp = "127.0.0.1";
	uint16_t servport = SERV_PORT;

	if (argc < 3) {
		//change servaddr
		cout << "Usage: main_client <nickname> <roomid>" << endl;
		return 1;
	}

	string nickname(*++argv);
	int roomid = atoi(*++argv);
	ChatClient cli(saddrp.c_str(), servport, nickname, roomid);
	cli.ConnServ(saddrp.c_str(), servport);
	
	return 0;
}